#
#    ||''\   ^   //''\ ||  | ||''.  //'.
#    ||--<  //\  ``--. ||--| ||-+' ||   
#  o ||__/ //''\ \\__/ ||  | ||  \  \\_/
#
# Description:              Bash run commands
# Dependencies:             bash
# Optionnal dependencies:   none
# Author:                   gawlk
# Contributors:             none

# ---
# DEFAULTS
# ---

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# ---
# PROMPT
# ---

PS1='\w \[\e[31m\]-\[\e[0m\] '

# ---
# PATHS
# ---

PATH=$PATH:${HOME}/.art
PATH=$PATH:${HOME}/.bin
PATH=$PATH:${HOME}/.libs
PATH=$PATH:${HOME}/.config/lemonbar

# ---
# SETTERS
# ---

# Set title of the window (used in lamp)
PROMPT_COMMAND='echo -en "\033]0;$(tty)\007"'
DISABLE_AUTO_TITLE=true

# Set config directory
XDG_CONFIG_HOME="$HOME/.config"

# Set default editor
export EDITOR=nvim

# ---
# ALIASES
# ---

0x0() { curl -F "file=@$1" http://0x0.st; }
alias clean_nvim='rm -R ${HOME}/.local/share/nvim/swap'
alias flip='[[ "$(xrandr | grep " connected " | cut -d" " -f5)" == "inverted" ]] && xrandr -o normal || xrandr -o inverted'
alias mem='sudo ps_mem'
alias mnt='sudo mount -o gid=users,fmask=113,dmask=002 /dev/sda1 ${HOME}/.mnt/usb'
alias reboot='sudo reboot'
alias shut='sudo shutdown -h now'
alias umnt='sudo umount .mnt/usb'

# ---
# COLORS
# ---

# Aliases
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ls='ls -a --color=auto'


# Man
export MANROFFOPT='-c'
export LESS_TERMCAP_mb=$(tput bold; tput setaf 2)
export LESS_TERMCAP_md=$(tput bold; tput setaf 6)
export LESS_TERMCAP_me=$(tput sgr0)
export LESS_TERMCAP_so=$(tput bold; tput setaf 3; tput setab 4)
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 7)
export LESS_TERMCAP_ue=$(tput rmul; tput sgr0)
export LESS_TERMCAP_mr=$(tput rev)
export LESS_TERMCAP_mh=$(tput dim)
