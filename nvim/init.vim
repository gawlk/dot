" Plugins
call plug#begin('~/.local/share/nvim/plugged')

Plug 'dylanaraps/wal.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'

call plug#end()

" Load wal scheme
colorscheme wal

" Minimal look
set nonu
set noru
set ls=0

" Linebreak
set linebreak

" Change tab to 4 spaces
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab
