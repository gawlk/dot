#define MOD             XCB_MOD_MASK_4
#define CURSOR_POSITION MIDDLE
#define LOOK_INTO       "WM_NAME"
#define WORKSPACES      2

static const bool resize_by_line = true;
static const bool inverted_colors = false;
static const float resize_keep_aspect_ratio = 1.5;

static const char *ignore_names[] = { "bar" };

static const char *binc[] = { "brit", "+", NULL };
static const char *bdec[] = { "brit", "-", NULL };
static const char *lamp[] = { "lamp", NULL };
static const char *rofi[] = { "rofi", "-show", "run", NULL };
static const char *term[] = { "lamp", "-t", "st", NULL };

static const uint16_t movements[] = { 
    0,  //step slow
    32, //step normal
    0,  //mouse slow
    0   //mouse normal
};

static const uint8_t offsets[] = { 
    0, //offsetx
    0, //offsety
    0, //maxwidth
    0  //maxheight
};

// Current colors are in .Xresources
static const char *colors[] = {
    "#ffffff", //focuscol
    "#ffffff", //unfocuscol
    "#ffffff", //fixedcol
    "#ffffff", //unkilcol
    "#ffffff", //fixedunkilcol
    "#ffffff", //outerbordercol
    "#ffffff"  //emptycol
};

// Borders sizes too
static const uint8_t borders[] = { 
    6, //outer border size
    8, //full border size
    2, //magnet border size
    2  //resize border size
};

static void killandfocus(const Arg *arg)
{
    deletewin(arg);
    Arg arg3 = {.i=TWOBWM_FOCUS_PREVIOUS};
    focusnext(&arg3);
}

static void sendandfocus(const Arg *arg)
{
    sendtoworkspace(arg);
    Arg arg2 = {.i=TWOBWM_FOCUS_PREVIOUS};
    focusnext(&arg2);
}

#define DESKTOPCHANGE(K,N) \
    {  MOD,                   K,             changeworkspace,     {.i=N} }, \
    {  MOD | SHIFT,           K,             sendandfocus,        {.i=N} },

static key keys[] = {
    // modifier               key            function             argument 
    
    // Focus to next window
    { MOD,                    XK_Tab,        focusnext,           {.i = TWOBWM_FOCUS_NEXT} },
    // Full screen window without borders
    { MOD,                    XK_f,          maximize,            {.i = TWOBWM_FULLSCREEN} },
    // Kill a window
    { MOD,                    XK_q,          killandfocus,        {.i = TWOBWM_FOCUS_PREVIOUS} },
    
    // Move a window
    { MOD,                    XK_Up,         movestep,            {.i = TWOBWM_MOVE_UP} },
    { MOD,                    XK_Down,       movestep,            {.i = TWOBWM_MOVE_DOWN} },
    { MOD,                    XK_Right,      movestep,            {.i = TWOBWM_MOVE_RIGHT} },
    { MOD,                    XK_Left,       movestep,            {.i = TWOBWM_MOVE_LEFT} },
    // Resize a window
    { MOD | SHIFT,            XK_Up,         resizestep,          {.i = TWOBWM_RESIZE_UP} },
    { MOD | SHIFT,            XK_Down,       resizestep,          {.i = TWOBWM_RESIZE_DOWN} },
    { MOD | SHIFT,            XK_Right,      resizestep,          {.i = TWOBWM_RESIZE_RIGHT} },
    { MOD | SHIFT,            XK_Left,       resizestep,          {.i = TWOBWM_RESIZE_LEFT} },

    // Center
    { MOD,                    XK_g,          teleport,            {.i = TWOBWM_TELEPORT_CENTER} },
    // Maximize left
    { MOD,                    XK_y,          maxhalf,             {.i = TWOBWM_MAXHALF_VERTICAL_LEFT} },
    // Maximize right
    { MOD,                    XK_u,          maxhalf,             {.i = TWOBWM_MAXHALF_VERTICAL_RIGHT} },

    // Restart 2bwm
    { MOD,                    XK_r,          twobwm_restart,      {.i = 0} },
    
    //Launch programs 
    { MOD,                    XK_Return,     start,               {.com = term} },
    { MOD,                    XK_d,          start,               {.com = rofi} },
    { MOD,                    XK_b,          start,               {.com = binc} },
    { MOD | SHIFT,            XK_b,          start,               {.com = bdec} },
    
    // Change current workspace
    DESKTOPCHANGE( XK_1, 0 )
    DESKTOPCHANGE( XK_2, 1 )
};

static Button buttons[] = { };
