![preview](preview.png)

# Setup

- Operating system: `void linux`
- Application Launcher: `rofi`
- Bar: `lemonbar`
- File manager: `ranger`
- Image viewer: `sxiv`
- Notifications: `notf (lemonbar)`
- Shell: `bash`
- Terminal Emulator: `st`
- Text Editor: `neovim`
- Video Player: `mpv`
- Web Browser: `tor browser`
- Window Manager: `2bwm`
